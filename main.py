
from telegram.ext import CommandHandler, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Bot
from telegram.ext import Updater, CommandHandler, MessageHandler,    Filters, InlineQueryHandler
import hashlib
import os

############################### Bot ############################################
def start(update, context):
    update.message.reply_text(main_menu_message(),
                            reply_markup=main_menu_keyboard())

def main_menu(update,context):
    query = update.callback_query
    query.answer()
    query.edit_message_text(
                        text=main_menu_message(),
                        reply_markup=main_menu_keyboard())

############################ Keyboards #########################################
def main_menu_keyboard():
    keyboard = []
    for datafile in os.listdir(data_path):
        filename = os.path.join(data_path, datafile)
        if not os.path.isdir(filename):
            keyboard.append([InlineKeyboardButton(filename.split("/")[-1], callback_data=str(hash(filename)))])
    keyboard += [[InlineKeyboardButton('Main menu', callback_data='main')]]
    return InlineKeyboardMarkup(keyboard, )


############################# Messages #########################################
def main_menu_message():
    return 'Выберите тип отчета, который бы хотели получать'

def timer_message():
    return 'Выберите периодичность обновлений'

def return_filename(filename):
    def return_file(update, context):
        context.bot.sendDocument(update['callback_query']['message']['chat']['id'], open(filename, 'rb'))
    return return_file

def timer_keyboard():
    keyboard = [[InlineKeyboardButton('5', callback_data="timer_5sec")],
                [InlineKeyboardButton('15', callback_data="timer_15sec")]]
    return InlineKeyboardMarkup(keyboard, )


def timer_generator(period):
    def callback_timer(bot, update, job_queue):
        query = update.callback_query
        query.answer()
        query.edit_message_text(
            text=timer_message(),
            reply_markup=timer_keyboard())
        bot.send_message(chat_id=update.message.chat_id,
                         text='Starting!')
        job_queue.run_repeating(callback_alarm, period, context=update.message.chat_id)
    return callback_timer


def callback_alarm(bot, job):
    bot.send_message(chat_id=job.context, text='Alarm' + str(time()))


def stop_timer(bot, update, job_queue):
    bot.send_message(chat_id=update.message.chat_id,
                     text='Stoped!')
    job_queue.stop()


def start(bot, update):
    query = update.callback_query
    menu_main = [[InlineKeyboardButton('Присылать апдейт раз в 5 секунд', callback_data='m1')],
                 [InlineKeyboardButton('Присылать апдейт раз в 15 секунд', callback_data='m2')]]
    reply_markup = InlineKeyboardMarkup(menu_main)
    bot.edit_message_text(chat_id=query.message.chat_id,
        message_id = query.message.message_id, text = 'Choose the option:',
        reply_markup = reply_markup)

def menu_actions(bot, update):
    query = update.callback_query()

    if query.data == 'm1':
        # first submenu
        menu_1 = [[InlineKeyboardButton('Submenu 1-1', callback_data='m1_1')],
                  [InlineKeyboardButton('Submenu 1-2', callback_data='m1_2')]]
        reply_markup = InlineKeyboardMarkup(menu_1)
        bot.edit_message_text(chat_id=query.message.chat_id,
                              message_id=query.message.message_id,
                              text='Choose the option:',
                              reply_markup=reply_markup)
    elif query.data == 'm2':
        # second submenu
        # first submenu
        menu_2 = [[InlineKeyboardButton('Submenu 2-1', callback_data='m2_1')],
                  [InlineKeyboardButton('Submenu 2-2', callback_data='m2_2')]]
        reply_markup = InlineKeyboardMarkup(menu_2)
        bot.edit_message_text(chat_id=query.message.chat_id,
                              message_id=query.message.message_id,
                              text='Choose the option:',
                              reply_markup=reply_markup)


# handlers

with open("token", "r") as token_file:
    token = token_file.readline()

updater = Updater(token, use_context=True)
updater.dispatcher.add_handler(CommandHandler('start', start))
updater.dispatcher.add_handler(CallbackQueryHandler(menu_actions))

updater.start_polling()
